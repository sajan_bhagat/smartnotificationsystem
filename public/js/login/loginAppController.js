var loginAppController=angular.module("loginAppController",[])

loginAppController.controller("parentController",function($scope,$element,$http){
	$scope.userCredentials={
		username:'',
		password:''
	}

	$element.find('.glyphicon-remove').on('click',function(){
		$element.find('.alert-danger').toggle();
	})

	$scope.loginSubmit=function(isValid) {
		if(isValid){
			var data=$scope.userCredentials;
			$http({
				method:'POST',
				url:"svc/login",
				data:data
			}).then(function successCallback(response){
				if(response.status==200&&response.data.status=='OK'){
					location.href="/i/dashboard";
				}else{
					$element.find('.alert-danger').toggle();
					// console.log(response.data);
				}
			},function errorCallback(response){
				// console.log(response.data);
				$element.find('.alert-danger').toggle();
			});
		}else{
			$element.find('.alert-danger').toggle();
		}
	}
})