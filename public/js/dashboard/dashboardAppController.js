var dashboardAppController=angular.module("dashboardAppController",['yaru22.angular-timeago'])

dashboardAppController.controller("parentController",function($scope,$element,$http,$interval,$filter){
	$scope.user=user;
	$scope.friendSuggestions=[];
	$scope.statusList=[];
	$scope.notifications=[];
	$scope.rid=null;
	$scope.globalSubscribed=null;
	
	$scope.status={
		statusMessage:'',
		userId:$scope.user.id,
		name:$scope.user.name,
		imageUrl:$scope.user.userInfo.imageUrl
	}

	$scope.csoon=function(){
		swal("This feautre will be enabled soon");
	}

	
	$scope.postStatus=function(isvalid){
		if(isvalid){
			$http({
				method:'POST',
				url:"/svc/updateStatus",
				data:$scope.status
			}).then(function successCallback(response){
				if(response.status==200&&response.data.status=='OK'){
					// console.log(response.data.result);
					$scope.statusList.unshift(response.data.result);
					$scope.status.statusMessage='';
				}else{
					console.log(response.data);
					
				}
			},function errorCallback(response){
				console.log(response.data);
				
			});
		}
	}

	$scope.getStatus=function(){
		$http({
			method:'GET',
			url:"/svc/getStatus",
			params:{
				userId:$scope.user.id
			}
		}).then(function successCallback(response){
			if(response.status==200&&response.data.status=='OK'){
				$scope.statusList=response.data.statusMessage;
			}else{
				$scope.statusList=[];
				console.log(response.data);
			}
		},function errorCallback(response){
			$scope.statusList=[];
			console.log(response.data);
		});
	}();

	$scope.getFriendSuggestions=function(){
		$http({
			method:'GET',
			url:"/svc/getFriendSuggestions",
			params:{
				userId:$scope.user.id
			}
		}).then(function successCallback(response){
			if(response.status==200&&response.data.status=='OK'){
				$scope.friendSuggestions=response.data.users;
			}else{
				console.log(response.data);
				$scope.friendSuggestions=[];
			}
		},function errorCallback(response){
			console.log(response.data);
			$scope.friendSuggestions=[];
		});
	}();

	$scope.getNotifications=function(obj,cb){
		$http({
			method:'GET',
			url:"/svc/getNotifications",
			params:{
				subsciberId:(obj&&obj.subsciberId)?obj.subsciberId:$scope.user.id,
				rid:(obj&&obj.rid)?obj.rid:''
			}
		}).then(function successCallback(response){
			if(response.status==200&&response.data.status=='OK'){
				cb(null,response);
			}else{
				cb(true,response);
			}
		},function errorCallback(response){
			cb(true,response);
		});		
	};

	$scope.getNotifications({
		subsciberId:$scope.user.id,
		rid:$scope.rid?$scope.rid:''
	},function(error,response){
		if(!error){
			$scope.notifications=response.data.notifications;
			$scope.rid=response.data.rid;
		}else{
			$scope.notifications=[];
			$scope.rid=null;
			console.log(response.data);
		}
	})

	$interval(function(){
		$scope.getNotifications({
			subsciberId:$scope.user.id,
			rid:$scope.rid?$scope.rid:''
		},function(error,response){
			if(!error){
				var notifications=response.data.notifications;
				if(notifications.length){
					for(i=0;i<notifications.length;i++){
						$scope.notifications.unshift(response.data.notifications[i]);	
					}
				}
				$scope.rid=response.data.rid;
			}else{
				console.log(response.data);
			}
		})
	},15000)

	$scope.dateFormat=function(ts){
		var d=new Date(ts);
		var ds=d.toDateString()+'('+d.toLocaleTimeString()+')'
		return ds;
	}

	$scope.timeDiff=function(ts){
		var yearDiff = new Date(new Date() - new Date(ts));
		var dateString=yearDiff.getHours()+' hrs '+yearDiff.getMinutes()+' mins ';
		
		if(yearDiff.getHours()>=24){
			return yearDiff.getDate() +'days'
		}else{
			return dateString;
		}

	}

	$scope.toggleSubscription=function(){
		if($scope.globalSubscribed){
			swal({
			  title: "Are you sure?",
			  text: "You will miss the all updates on gossip girls!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes, unsubscribe it!",
			  cancelButtonText: "Cancel",
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm) {
			  if (isConfirm) {
				$scope.globalUnsubscribe()
			    swal("Deleted!", "You Unsubscribed Global Updates", "success");
			  } else {
			    swal("Cancelled", "you will be notified for every update on gossip girls! :)", "error");
			  }
			});
		}else{
			swal({
			  title: "Are you sure?",
			  text: "You will be notified for every update on gossip girls!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes, Subsribe it!",
			  cancelButtonText: "Cancel",
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm) {
			  if (isConfirm) {
			  	$scope.globalSubscribe()
			    swal("Deleted!", "You Subscribed global updates .", "success");
			  } else {
			    swal("Cancelled", "You are not Subscribed to Global updates", "error");
			  }
			});
		}
	}

	$scope.globalSubscribe=function(){

		var data={
			subscribeUserId:$scope.user.id,
			globalSubscribe:true
		}

		$http({
			method:'POST',
			url:"/svc/subscribeUser",
			data:data
		}).then(function successCallback(response){
			if(response.status==200&&response.data.status=='OK'){
				$scope.globalSubscribed=true;
			}else{
				console.log(response.data);
			}
		},function errorCallback(response){
			console.log(response.data);
		});

	}

	$scope.globalUnsubscribe=function(){
		var data={
			subscribeUserId:$scope.user.id,
			globalUnSubscribe:true
		}
		
		$http({
			method:'POST',
			url:"/svc/unsubscribeUser",
			data:data
		}).then(function successCallback(response){
			if(response.status==200&&response.data.status=='OK'){
				$scope.globalSubscribed=false;
			}else{
				console.log(response.data);
			}
		},function errorCallback(response){
			console.log(response.data);
		});

	}

	$scope.getGlobalSubscriptionStatus=function(){
		
		var data={
			subscribeUserId:$scope.user.id
		};

		$http({
			method:'GET',
			url:"/svc/getGlobalSubscriptionStatus",
			params:data
		}).then(function successCallback(response){
			if(response.status==200&&response.data.status=='OK'){
				$scope.globalSubscribed=response.data.subscription;
			}else{
				console.log(response.data);
			}
		},function errorCallback(response){
			console.log(response.data);
		});

	}();

})

dashboardAppController.controller("headerController",function($scope,$element,$http){
	$scope.logout=function() {
		$http({
			method:'GET',
			url:"/svc/logout"
		}).then(function successCallback(response){
			if(response.status==200&&response.data.status=='OK'){
				location.href="/";
			}else{
				console.log(response.data);
			}
		},function errorCallback(response){
			console.log(response.data);
		});
	}

	$element.find('div.link-ds').on('click',function(){
		location.href='/'
	})
})

