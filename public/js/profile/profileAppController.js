var profileAppController=angular.module("profileAppController",[])

profileAppController.controller("parentController",function($scope,$element,$http){
	$scope.user=user;
	$scope.subscribed=null;
	$scope.visitedUser=visitedUser;
	// console.log(user,visitedUser);
	$scope.csoon=function(){
		swal("This feautre will be enabled soon");
	}
	
	$scope.subscribeUser=function(){
		
		var data={
			userId:$scope.visitedUser.id,
			subscribeUserId:$scope.user.id,
			globalSubscribe:false
		};

		if(!$scope.subscribed){
			swal({
			  title: "Are you sure?",
			  text: "You will be notified for all status updates!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes, subscribe it!",
			  cancelButtonText: "Cancel",
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm) {
			  if (isConfirm) {
				  	$http({
						method:'POST',
						url:"/svc/subscribeUser",
						data:data
					}).then(function successCallback(response){
						if(response.status==200&&response.data.status=='OK'){
							$scope.subscribed=!$scope.subscribed;
							swal("Deleted!", "You subscribed status updates", "success");
						}else{
							console.log(response.data);
						}
					},function errorCallback(response){
						console.log(response.data);
					});
			  } else {
			    swal("Cancelled", "You are not subscribed to status updates", "error");
			  }
			});
		}else{
			swal({
			  title: "Are you sure?",
			  text: "You will not get notified for all status updates!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes, UnSubscribe it!",
			  cancelButtonText: "Cancel",
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm) {
			  if (isConfirm) {
				  	$http({
						method:'POST',
						url:"/svc/unsubscribeUser",
						data:data
					}).then(function successCallback(response){
						if(response.status==200&&response.data.status=='OK'){
							$scope.subscribed=!$scope.subscribed;
							swal("Deleted!", "You UnSubscribed status updates", "success");
						}else{
							console.log(response.data);
						}
					},function errorCallback(response){
						console.log(response.data);
					});
			  } else {
			    swal("Cancelled", "You are subscribed to status updates", "error");
			  }
			});
		}
	}

	$scope.getSubscriptionStatus=function(){
		
		var data={
			userId:$scope.visitedUser.id,
			subscribeUserId:$scope.user.id
		};

		$http({
			method:'GET',
			url:"/svc/getSubscriptionStatus",
			params:data
		}).then(function successCallback(response){
			if(response.status==200&&response.data.status=='OK'){
				$scope.subscribed=response.data.subscription;
			}else{
				console.log(response.data);
			}
		},function errorCallback(response){
			console.log(response.data);
		});

	}();

})

profileAppController.controller("headerController",function($scope,$element,$http){
	$scope.logout=function() {
		$http({
			method:'GET',
			url:"/svc/logout"
		}).then(function successCallback(response){
			if(response.status==200&&response.data.status=='OK'){
				location.href="/";
			}else{
				console.log(response.data);
			}
		},function errorCallback(response){
			console.log(response.data);
		});
	}

	$element.find('div.link-ds').on('click',function(){
		location.href='/'
	})

})