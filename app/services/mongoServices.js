var http = require('http');
var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var MODELS=require(global.MODEL_PATH+'__models');

var user=MODELS.user;
var status=MODELS.status;
var subscription=MODELS.subscription;
var notification=MODELS.notification;

function mongoservices(){
}

mongoservices.prototype.updateStatus = function(obj,cb) {
	var newStatus=new status(obj);
	newStatus.timeStamp=new Date().getTime();
	newStatus.save(function (err,result) {
		if(!err){
			cb(null,{
				status:'OK',
				result:result
			});
		}else{
			console.log(err,'######');
		}
	})
};

mongoservices.prototype.getStatus=function(obj,cb){
	status.find({userId:obj.userId,statusType:'status'},function(err,status){
		if (!err) {
			cb(null,{
				status:'OK',
				statusMessage:status
			});
		}else{
			cb(err);
		}
	}).sort( { timeStamp: -1 } ).limit(5);
}

mongoservices.prototype.getUserInfo=function(obj,cb){
	var id = mongoose.Types.ObjectId(obj.id);
	user.findById(id,function(err,user){
		if(!err){
			var result={};
			result.userInfo=user.userInfo;
			result.name=user.name;
			result.email=user.credentials.email;
			result.id=user._id;
			cb(null,result);
		}else{
			cb(err);
		}
	})
}

mongoservices.prototype.getFriendSuggestions=function(obj,cb){
	var id = mongoose.Types.ObjectId(obj.userId);
	user.find({_id:{$ne:id}},{userInfo:1,name:1,'credentials.email':1},function(err,users){
		if(!err){
			cb(null,{
				status:'OK',
				users:users
			})
		}else{
			cb(err)
		}
	})
}

mongoservices.prototype.subscribeUser=function(obj,cb){
	subscription.findById(obj.userId,function(e,result){
		if(!e){
			if(!result){
				var newSubscription=new subscription({
					_id:obj.userId,
					userId:obj.userId,
					subscribers:[obj.subscribeUserId]
				})
				newSubscription.save(function(error,subscription){
					if(!error){
						cb(null,{
							status:'OK',
							result:'subscribed to user'
						});
					}else{
						cb(error);
					}
				})				
			}else{
				subscription.update({_id:obj.userId},{$addToSet:{subscribers:obj.subscribeUserId}},function(err,res){
					if(!err){
						cb(null,{
							status:'OK',
							result:'subscribed to user'
						});
					}else{
						cb(err);
					}
				})
			}
		}else{
			cb(e);
		}
	})
}

mongoservices.prototype.unsubscribeUser=function(obj,cb){
	subscription.findById(obj.userId,function(e,result){
		if(!e){
			if(!result){
					cb(null,{
						status:'OK',
						message:'unscribed user'
					})						
			}else{
				subscription.update({_id:obj.userId},{$pull:{subscribers:obj.subscribeUserId}},function(error,newSubscription){
					if(!error){
						cb(null,{
							status:'OK',
							message:'unscribed user'
						})						
					}else{
						cb(error);
					}
				})
			}
		}else{
			cb(e);
		}
	})
}

mongoservices.prototype.subscribeGlobal=function(obj,cb){
	var __self=this;

	subscription.findOne({_id:'global_user'},function(err,subscriptionExists){
		if(!err){
			if(subscriptionExists){
				subscription.update({_id:'global_user'},{$addToSet:{subscribers:obj.subscribeUserId}},function(error,subscriptionNew){
					if(!error){
						cb(null,{
							status:'OK',
							message:'global subscription done'
						})
					}else{
						cb(error);
					}
				})
			}else{
				var newSubscription=subscription({_id:'global_user',userId:'global_user',subscribers:[obj.subscribeUserId]});
				newSubscription.save(function(error,subscriptionNew){
					if(!error){
						cb(null,{
							status:'OK',
							message:'global subscription done'
						})
					}else{
						cb(error);
					}
				})
			}
		}else{
			cb(err);
		}
	});
}

mongoservices.prototype.unsubscribeGlobal=function(obj,cb){
	subscription.findOne({_id:'global_user'},function(error,subscriptionExists){
		if(!error){
			if(subscriptionExists){
				subscription.update({_id:'global_user'},{$pull:{subscribers:obj.subscribeUserId}},function(err,result){
					if(!err){
						cb(null,{
							status:'OK',
							message:'UnSubscribed All Users'
						});
					}else{
						cb(err);
					}
				})
			}else{
				cb(null,{
					status:'OK',
					message:'You are not subscribed globaly'
				});
			}
		}else{
			cb(error)
		}
	})
}

mongoservices.prototype.getSubscriptionStatus=function(obj,cb){
	subscription.findOne({
		_id:obj.userId,
		subscribers:{
			$in:[obj.subscribeUserId]
		}
	},function(error,result){
		if(!error){
			if(result){
				cb(null,{
					status:'OK',
					subscription:true
				});	
			}else{
				cb(null,{
					status:'OK',
					subscription:false
				})
			}
		}else{
			cb(error);
		}
	})
}

mongoservices.prototype.getGlobalSubscriptionStatus=function(obj,cb){
	subscription.findOne({
		_id:'global_user',
		subscribers:{
			$in:[obj.subscribeUserId]
		}
	},function(error,result){
		if(!error){
			if(result){
				cb(null,{
					status:'OK',
					subscription:true
				});	
			}else{
				cb(null,{
					status:'OK',
					subscription:false
				})
			}
		}else{
			cb(error);
		}
	})
}


mongoservices.prototype.pushNotification = function(obj,cb) {
	var __self=this;

	var subscribers=obj.subscribers;
	var status=obj.status;
	var notificationType=obj.notificationType;

	__self.count=0;
	__self.error=false;

	/* callback counter*/
	
	subscribers=subscribers.filter(function(element){
		/* preventing message to be broadcated to yourself */
		if(element!=status.userId)
			return element;
	})

	// console.log(subscribers);

	for(i=0;i<subscribers.length;i++){
		
		var newNotification=new notification({
			status:status,
			subsciberId:subscribers[i],
			notificationType:notificationType
		})

		newNotification.save(function(error,result){
			if(!error){
				__self.count++;
			}else{
				__self.count++;
				__self.error=error;
			}

			if(__self.count==subscribers.length){
				if(!__self.error){
					cb(null,{
						message:'notification created successfully'
					});
				}else{
					cb(__self.error);
				}
			}

		})
	}
};

mongoservices.prototype.getNotifications=function(obj,cb){
	if(!obj.rid){
		notification.find({subsciberId:obj.subsciberId},function(error,notifications){
			if(!error){
				if(!notifications.length){
					cb(null,{
						status:'OK',
						notifications:[],
						rid:''
					});
				}else{
					cb(null,{
						status:'OK',
						notifications:notifications,
						rid:notifications[0].status.timeStamp
					});
				}
			}else{
				cb(error)
			}
		}).sort({"status.timeStamp":-1}).limit(10);
	}else{
		notification.find({subsciberId:obj.subsciberId,'status.timeStamp':{$gt:obj.rid}},function(error,notifications){
			if(!error){
				if(!notifications.length){
					cb(null,{
						status:'OK',
						notifications:[],
						rid:obj.rid
					});
				}else{
					cb(null,{
						status:'OK',
						notifications:notifications,
						rid:notifications[0].status.timeStamp
					});
				}
			}else{
				cb(error);
			}
		}).sort({"status.timeStamp":1});
	}
}

var __this=new mongoservices();
module.exports=mongoservices;