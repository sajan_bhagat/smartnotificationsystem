var mongoose=require('mongoose');
var Schema=mongoose.Schema;
// var shortid = require('shortid');

var subscriptionSchema=new Schema({
	_id: {
	    'type': String
	    // 'default': shortid.generate
	},
	userId:String,
	subscribers:[String]
})

var subscription = mongoose.model('subscription', subscriptionSchema);
module.exports=subscription;