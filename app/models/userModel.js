var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var shortid = require('shortid');

var userSchema=new Schema({
	// _id: {
	//     'type': String,
	//     'default': shortid.generate
	// },
    "name" : String,
    "userInfo" : {
        "DOB" : String,
        "nick-name" : String,
        "phoneNo":Number,
        "address":String,
        "location":[Number,Number],
        "imageUrl":String
    },
    "credentials" : {
        "email" : String,
        "password" : String,
        "username" : String
    }
});

var user = mongoose.model('user', userSchema);

module.exports =  user ;