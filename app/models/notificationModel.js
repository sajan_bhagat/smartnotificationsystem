var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var shortid = require('shortid');

var notificationSchema=new Schema({
	_id: {
	    'type': String,
	    'default': shortid.generate
	},
	status:{
		_id: String,
		userId:String,
		statusMessage:String,
		timeStamp:Number,
		name:String,
		imageUrl:String,
		statusType:String,
		statusArray:[String]
	},
	notificationType:String,
	subsciberId:String,
	readStatus:{
	    'type': Boolean,
	    'default': false
	}
})


var notification = mongoose.model('notification', notificationSchema);
module.exports=notification;