var user=require('./userModel');
var subscription=require('./subscriptionModel');
var status=require('./statusModel');
var notification=require('./notificationModel')

var MODELS={
	user:user,
	subscription:subscription,
	status:status,
	notification:notification
}

module.exports=MODELS;