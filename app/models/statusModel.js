var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var shortid = require('shortid');

var statusSchema=new Schema({
	_id: {
	    'type': String,
	    'default': shortid.generate
	},
	userId:String,
	statusMessage:String,
	timeStamp:Number,
	name:String,
	imageUrl:String,
	statusArray:[String],
	statusType:{
		'type': String,
	    'default': 'status'
	}
})

var status = mongoose.model('status', statusSchema);
module.exports=status;