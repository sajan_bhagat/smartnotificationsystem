var locomotive = require('locomotive')
  , Controller = locomotive.Controller;

var mongoServices=require(global.SVC_PATH+'mongoServices');
var dbServices=new mongoServices();

var updateStatusController = new Controller();

updateStatusController.updateStatus=function(){
	var __self=this;
	var obj=__self.req.body;
	dbServices.updateStatus(obj,function(err,result) {
		if(!err){
			__self.res.write(JSON.stringify(result));
			__self.res.end();
		}else{
			__self.res.write(JSON.stringify({
				status:'ERROR',
				error:err?err.toString():'invalid input'
			}));
			__self.res.end();
		}
	})
}


module.exports=updateStatusController;