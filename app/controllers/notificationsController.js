var locomotive = require('locomotive')
  , Controller = locomotive.Controller;

var mongoServices=require(global.SVC_PATH+'mongoServices');
var dbServices=new mongoServices();

var notificationController = new Controller();

notificationController.getNotifications=function(){
	var __self=this;
	var rid=__self.req.query.rid?__self.req.query.rid:__self.req.body.rid;
	var subsciberId=__self.req.query.subsciberId?__self.req.query.subsciberId:__self.req.body.subsciberId;
	/* request id as timestamp */

	dbServices.getNotifications({
		rid:rid?rid:null,
		subsciberId:subsciberId
	},function(err,result) {
		if(!err){
			__self.res.write(JSON.stringify(result));
			__self.res.end();
		}else{
			__self.res.write(JSON.stringify({
				status:'ERROR',
				error:err?err.toString():'invalid input'
			}));
			__self.res.end();
		}
	})

}


module.exports=notificationController;