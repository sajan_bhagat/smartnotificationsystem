var locomotive = require('locomotive')
  , Controller = locomotive.Controller;

var mongoServices=require(global.SVC_PATH+'mongoServices');
var dbServices=new mongoServices();

var subscribeUserController = new Controller();

subscribeUserController.subscribeUser=function(){
	var __self=this;
	var userId=__self.req.query.userId?__self.req.query.userId:__self.req.body.userId;
	var subscribeUserId=__self.req.query.subscribeUserId?__self.req.query.subscribeUserId:__self.req.body.subscribeUserId;
	var globalSubscribe=__self.req.query.globalSubscribe?__self.req.query.globalSubscribe:__self.req.body.globalSubscribe;

	if(!globalSubscribe){
		dbServices.subscribeUser({
			userId:userId,
			subscribeUserId:subscribeUserId
		},function(err,result) {
			if(!err){
				__self.res.write(JSON.stringify(result));
				__self.res.end();
			}else{
				__self.res.write(JSON.stringify({
					status:'ERROR',
					error:err?err.toString():'invalid input'
				}));
				__self.res.end();
			}
		})
	}else{
		dbServices.subscribeGlobal({
			subscribeUserId:subscribeUserId
		},function(err,result){
			if(!err){
				__self.res.write(JSON.stringify(result));
				__self.res.end();
			}else{
				__self.res.write(JSON.stringify({
					status:'ERROR',
					error:err?err.toString():'invalid input'
				}));
				__self.res.end();
			}
		})
	}
}

subscribeUserController.unsubscribeUser=function(){
	var __self=this;
	var userId=__self.req.query.userId?__self.req.query.userId:__self.req.body.userId;
	var subscribeUserId=__self.req.query.subscribeUserId?__self.req.query.subscribeUserId:__self.req.body.subscribeUserId;
	var globalUnSubscribe=__self.req.query.globalUnSubscribe?__self.req.query.globalUnSubscribe:__self.req.body.globalUnSubscribe;

	if(!globalUnSubscribe){
		dbServices.unsubscribeUser({
			userId:userId,
			subscribeUserId:subscribeUserId
		},function(err,result) {
			if(!err){
				__self.res.write(JSON.stringify(result));
				__self.res.end();
			}else{
				__self.res.write(JSON.stringify({
					status:'ERROR',
					error:err?err.toString():'invalid input'
				}));
				__self.res.end();
			}
		})
	}else{
		dbServices.unsubscribeGlobal({
			subscribeUserId:subscribeUserId
		},function(err,result){
			if(!err){
				__self.res.write(JSON.stringify(result));
				__self.res.end();
			}else{
				__self.res.write(JSON.stringify({
					status:'ERROR',
					error:err?err.toString():'invalid input'
				}));
				__self.res.end();
			}
		})
	}
}

subscribeUserController.getSubscriptionStatus=function(){
	var __self=this;
	var userId=__self.req.query.userId?__self.req.query.userId:__self.req.body.userId;
	var subscribeUserId=__self.req.query.subscribeUserId?__self.req.query.subscribeUserId:__self.req.body.subscribeUserId;

	dbServices.getSubscriptionStatus({
		userId:userId,
		subscribeUserId:subscribeUserId
	},function(err,result) {
		if(!err){
			__self.res.write(JSON.stringify(result));
			__self.res.end();
		}else{
			__self.res.write(JSON.stringify({
				status:'ERROR',
				error:err?err.toString():'invalid input'
			}));
			__self.res.end();
		}
	})
}

subscribeUserController.getGlobalSubscriptionStatus=function(){
	var __self=this;
	var subscribeUserId=__self.req.query.subscribeUserId?__self.req.query.subscribeUserId:__self.req.body.subscribeUserId;

	dbServices.getGlobalSubscriptionStatus({
		subscribeUserId:subscribeUserId
	},function(err,result){
		if(!err){
			__self.res.write(JSON.stringify(result));
			__self.res.end();
		}else{
			__self.res.write(JSON.stringify({
				status:'ERROR',
				error:err?err.toString():'invalid input'
			}));
			__self.res.end();
		}	
	})
}

module.exports=subscribeUserController;