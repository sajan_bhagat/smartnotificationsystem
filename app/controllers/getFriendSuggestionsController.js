var locomotive = require('locomotive')
  , Controller = locomotive.Controller;

var mongoServices=require(global.SVC_PATH+'mongoServices');
var dbServices=new mongoServices();

var getFriendSuggestionsController = new Controller();

getFriendSuggestionsController.getFriendSuggestions=function(){
	var __self=this;
	var userId=__self.req.query.userId?__self.req.query.userId:__self.req.body.userId;
	dbServices.getFriendSuggestions({
		userId:userId
	},function(err,result) {
		if(!err){
			__self.res.write(JSON.stringify(result));
			__self.res.end();
		}else{
			__self.res.write(JSON.stringify({
				status:'ERROR',
				error:err?err.toString():'invalid input'
			}));
			__self.res.end();
		}
	})
}


module.exports=getFriendSuggestionsController;