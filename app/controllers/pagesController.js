var locomotive = require('locomotive')
  , Controller = locomotive.Controller;
var pagesController = new Controller();
var passport=require('passport');
var mongoServices=require(global.SVC_PATH+'mongoServices');
var dbServices=new mongoServices();

pagesController.main = function() {
	var __self=this;
	if(__self.req.isAuthenticated()){
		__self.redirect('/i/dashboard');
	}else{
		__self.title = 'GossipGirls';
  		__self.render();		
	}
}

pagesController.before('login',passport.authenticate('local'));

pagesController.login=function(){
	var __self=this;
	if(__self.req.isAuthenticated()){
		__self.res.write(JSON.stringify({
			status:'OK',
			result:{
				user:__self.req.user
			}
		}))
		__self.res.end();
	}
}

pagesController.logout=function(){
	var __self=this;
	if(__self.req.isAuthenticated()){
		__self.req.logout();
		__self.res.write(JSON.stringify({
			status:'OK',
			result:{
				message:'logged out successfully'
			}
		}))
		__self.res.end();
	}else{
		__self.res.write(JSON.stringify({
			status:'ERROR',
			message:'UnAuthorized access'
		}))
		__self.res.end();
	}
}

pagesController.dashboard=function(){
	var __self=this;
	if(__self.req.isAuthenticated()){
		var id=__self.req.user.id;
		dbServices.getUserInfo({id:id},function(error,result){
			if(!error)	{
				__self.user=result;
				__self.title=result.name;
				__self.render();		
			}else{
				__self.redirect('/');
			}
		})
	}else{
		__self.redirect('/');
	}
}

pagesController.profile=function(){
	var __self=this;
	var userId=__self.req.params.userId;

	if(__self.req.isAuthenticated()){
		var id=__self.req.user.id;
		dbServices.getUserInfo({id:id},function(error,result){
			if(!error)	{
				__self.user=result;
				__self.title=result.name;
				if(userId){
					dbServices.getUserInfo({id:userId},function(err,res){
						if(!err){
							__self.visitedUser=res;
							__self.render();
							
						}else{
							__self.redirect('/i/dashboard');
						}
					})			
				}else{
					__self.redirect('/i/dashboard');
				}
			}else{
				__self.redirect('/i/dashboard');
			}
		})
	}else{
		__self.redirect('/');
	}
}

pagesController.error404=function() {
	var __self=this;
	__self.render();
}

pagesController.error=function() {
	var __self=this;
	__self.redirect('/i/404');
}

module.exports = pagesController;
