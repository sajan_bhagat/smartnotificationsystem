var host="54.187.68.188";
var port='27017';
var dbName="smartNotifications";
var connectionString='mongodb://'+host+":"+port+'/'+dbName;

var mongoose = require('mongoose');
var MODELS=require(global.MODEL_PATH+'__models');
var user=MODELS.user;
var statusModel=MODELS.status;

var mongoServices=require(global.SVC_PATH+'mongoServices');
var mServices=new mongoServices();
var _=require('underscore');

// var redisClient=require(global.INIT_PATH+'_redis_init');

var subscription=MODELS.subscription;

var MongoOplog = require('mongo-oplog');
var oplog = MongoOplog(connectionString).tail();


oplog.filter('smartNotifications.status').on('insert', function (data) {

  	var status=data.o;
  	var subscribers=[];
    if(status.statusType!='info'){
      subscription.find({_id:{$in:[status.userId,'global_user']}},function(error,result){
          if(!error){
              if(result.length){
                  for(i=0;i<result.length;i++){
                      subscribers=_.union(subscribers,result[i].subscribers);
                  }
                  
                  var obj={
                     status:status,
                     subscribers:subscribers,
                     notificationType:status.statusType
                  }
                  
                  mServices.pushNotification(obj,function(e,res){
                     if(!e){
                       console.log(res);
                     }else{
                       console.log(res);
                     }
                  });
              }
          }
      })
    }else{

      subscription.findOne({_id:{$in:['global_user']}},function(error,result){
          if(!error){
              if(result){
                var obj={
                   status:status,
                   subscribers:result.subscribers,
                   notificationType:status.statusType
                }
                
                mServices.pushNotification(obj,function(e,res){
                   if(!e){
                     console.log(res);
                   }else{
                     console.log(res);
                   }
                });
              }
          }
      })

    }
});

oplog.filter('smartNotifications.users').on('update', function (data) {
  
    var userId=data.o2._id;
    var operation=data.o['$set'];
    var keys=Object.keys(operation);
    var statusMessage=[];
    
    for(i=0;i<keys.length;i++){
      if(keys[i].split('.')[0]!='credentials'){
        statusMessage.push('updated '+(keys[i].split('.')[1]?keys[i].split('.')[1]:keys[i].split('.')[0])+' to '+operation[keys[i]]);
      }
    }

    if(statusMessage.length){
      user.findOne({_id:userId},function(error,user){
        if(!error){

          var status={
            timeStamp:new Date().getTime(),
            statusArray:statusMessage,
            userId:user.id,
            name:user.name,  
            imageUrl:user.userInfo.imageUrl,
            statusMessage:'',
            statusType:'info'
          }

          var newStatus=new statusModel(status);
          
          newStatus.save(function(error,result){
              if(!error){
                console.log('info update status created');
              }else{
                console.log(error)
              }
          })

        }else{
          console.log(error);
        }
      })
    }
})