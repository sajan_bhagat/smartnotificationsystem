var mongoose = require('mongoose');
var host="54.187.68.188";
var port='27017';
var dbName="smartNotifications";

var connectionString='mongodb://'+host+":"+port+'/'+dbName;

mongoose.connect(connectionString);
var db = mongoose.connection;

db.on('error', function(error){
	console.log(error);
});

db.once('open', function() {
    console.log('\t\t ########################################### connected to smartNotification DB ###########################################\n');
	var obj={
		dbClient:db,
	  	mongoose:mongoose
	};
	
	module.exports={
	  	dbClient:db,
	  	mongoose:mongoose
	};
});