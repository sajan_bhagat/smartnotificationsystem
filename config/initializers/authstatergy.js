var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

var MODELS=require(global.MODEL_PATH+'__models');
var user=MODELS.user;

module.exports = function() {

  passport.serializeUser(function(user, done) {
    	// console.log('############### serializing user #########################');
      done(null, user.email);
  });

  passport.deserializeUser(function(id, done) {
      // console.log('############### deserializeUser user #########################');
      user.find({'credentials.email':id}, function(err, user) {
        if(!err&&user.length){
          var obj={
            id:user[0]._id,
            name:user[0].name,
            email:user[0].credentials['email'],
            phoneNo:user[0].userInfo['phoneNo']
          }
          done(null, obj);
        }else{
          done(null, false);
        }
      });
  });

  passport.use(new LocalStrategy(
    
    function(username, password, done) {
      user.find({'credentials.email':username,'credentials.password':password},function(err,user){
          if(!err&&user.length){
              var obj={
                id:user[0]._id,
                name:user[0].name,
                email:user[0].credentials['email'],
                phoneNo:user[0].userInfo['phoneNo']
              }
              return done(null,obj);
          }else{
              return done(null, false);
          }
      })
   	}
  ));

}