// Draw routes.  Locomotive's router provides expressive syntax for drawing
// routes, including support for resourceful routes, namespaces, and nesting.
// MVC routes can be mapped to controllers using convenient
// `controller#action` shorthand.  Standard middleware in the form of
// `function(req, res, next)` is also fully supported.  Consult the Locomotive
// Guide on [routing](http://locomotivejs.org/guide/routing.html) for additional
// information.

module.exports = function routes() {
  this.root('pages#main');
  this.match('svc/login',{controller:'pages',action:'login',via:'POST'});
  this.match('svc/logout',{controller:'pages',action:'logout',via:'GET'});
  
  this.match('i/dashboard',{controller:'pages',action:'dashboard',via:'GET'});
  this.match('i/profile/:userId',{controller:'pages',action:'profile',via:'GET'});
  this.match('i/404',{controller:'pages',action:'error404',via:'GET'});

  this.match('svc/updateStatus',{controller:'updateStatusController',action:'updateStatus',via:'POST'});
  this.match('svc/getStatus',{controller:'getStatusController',action:'getStatus',via:'GET'})
  this.match('svc/getFriendSuggestions',{controller:'getFriendSuggestionsController',action:'getFriendSuggestions',via:'GET'});
  this.match('svc/subscribeUser',{controller:'subscribeUserController',action:'subscribeUser',via:'POST'});
  this.match('svc/unsubscribeUser',{controller:'subscribeUserController',action:'unsubscribeUser',via:'POST'})
  this.match('svc/getSubscriptionStatus',{controller:'subscribeUserController',action:'getSubscriptionStatus',via:'GET'});
  this.match('svc/getGlobalSubscriptionStatus',{controller:'subscribeUserController',action:'getGlobalSubscriptionStatus',via:'GET'});
  this.match('svc/getNotifications',{controller:'notificationsController',action:'getNotifications',via:'GET'});

  // this.match('*',{controller:'pages',action:'error',via:'GET'});

}
